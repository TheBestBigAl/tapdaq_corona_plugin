local Library = require "CoronaLibrary"

-- Create library
local tapdaq = Library:new{ name='tapdaq', publisherId='com.tapdaq' }


tapdaq.init = function()
	native.showAlert("Unavailable on this format", "Tapdaq functions are not available in the Corona Simulator", {"OK"})
end

tapdaq.getPluginVersion = function()
	native.showAlert("Unavailable on this format", "Tapdaq functions are not available in the Corona Simulator", {"OK"})
end

tapdaq.showInterstitial = function()
	native.showAlert("Unavailable on this format", "Tapdaq functions are not available in the Corona Simulator", {"OK"})
end

tapdaq.showTraditional = function()
	native.showAlert("Unavailable on this format", "Tapdaq functions are not available in the Corona Simulator", {"OK"})
end

tapdaq.showBanner = function()
	native.showAlert("Unavailable on this format", "Tapdaq functions are not available in the Corona Simulator", {"OK"})
end

tapdaq.showMoreApps = function()
	native.showAlert("Unavailable on this format", "Tapdaq functions are not available in the Corona Simulator", {"OK"})
end

-------------------------------------------------------------------------------
-- END
-------------------------------------------------------------------------------

-- Return an instance
return tapdaq

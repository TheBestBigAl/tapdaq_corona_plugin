local metadata =
{
    plugin =
    {
        format = 'staticLibrary',
        staticLibs = { 'tapdaqLibrary', },
        frameworks = {},
        frameworksOptional = {},
    },
}

return metadata
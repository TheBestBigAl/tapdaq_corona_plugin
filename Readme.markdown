# Tapdaq Plugin

Please visit the [Tapdaq](https://www.tapdaq.com/) website to sign up for an account, and for more information (make sure not to miss the FAQs at the end).  
## Overview

This library allows access to the Tapdaq peer-to-peer advertising network.
You must call tapdaq.init() before calling any other functions.

## Sign Up

To use the Tapdaq service, please [sign up](http://tapdaq.com) for a Tapdaq account and set up your application.

## Platforms

* Android: No
* iOS: Yes
* Mac: No
* Win: No
* Kindle: No
* NOOK: No

### Enterprise

Add the libtapdaqLibrary.a file to your project and be sure to link it to your binary. No other setup should be required.

You can access sample code [here](https://bitbucket.org/TheBestBigAl/tapdaq_corona_plugin/src).

## Functions

#### [tapdaq.init()][plugin.tapdaq.init]
#### [tapdaq.getPluginVersion()][plugin.tapdaq.getPluginVersion]
#### [tapdaq.showInterstitial()][plugin.tapdaq.showInterstitial]
#### [tapdaq.showMoreApps()][plugin.tapdaq.showMoreApps]
#### [tapdaq.hasCachedInterstitial()][plugin.tapdaq.hasCachedInterstitial]
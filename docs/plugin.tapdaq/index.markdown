# tapdaq.*

> --------------------- ------------------------------------------------------------------------------------------
> __Type__              [library][api.type.library]
> __Revision__          [REVISION_LABEL](REVISION_URL)
> __Keywords__          tapdaq, ads
> __Sample code__       
> __See also__          
> __Availability__      Starter, Basic, Pro, Enterprise
> --------------------- ------------------------------------------------------------------------------------------

## Overview

This library allows access to the Tapdaq peer-to-peer advertising network.
You must call tapdaq.init() before calling any other functions.

## Sign Up

To use the Tapdaq service, please [sign up](http://tapdaq.com) for a Tapdaq account and set up your application.

## Platforms

* Android: No
* iOS: Yes
* Mac: No
* Win: No
* Kindle: No
* NOOK: No

## Syntax

local tapdaq = require "plugin.tapdaq"

## Functions

#### [tapdaq.init()][plugin.tapdaq.init]
#### [tapdaq.getPluginVersion()][plugin.tapdaq.getPluginVersion]
#### [tapdaq.showInterstitial()][plugin.tapdaq.showInterstitial]
#### [tapdaq.showMoreApps()][plugin.tapdaq.showMoreApps]
#### [tapdaq.hasCachedInterstitial()][plugin.tapdaq.hasCachedInterstitial]


## Project Settings

### SDK

When you build using the Corona Simulator, the server automatically takes care of integrating the plugin into your project. 

All you need to do is add an entry into a `plugins` table of your `build.settings`. The following is an example of a minimal `build.settings` file:

``````
settings =
{
	plugins =
	{
		-- key is the name passed to Lua's 'require()'
		["plugin.tapdaq"] =
		{
			-- required
			publisherId = "com.tapdaq",
		},
	},		
}
``````

### Enterprise

Add the libtapdaqLibrary.a file to your project and be sure to link it to your binary. No other setup should be required.

## Sample Code

You can access sample code [here](https://bitbucket.org/TheBestBigAl/tapdaq_corona_plugin/src).

## Support

More support is available from the Tapdaq team:

* [E-mail](mailto://support@tapdaq.com)
* [Forum](http://forum.coronalabs.com/plugin/tapdaq)
* [Plugin Publisher](http://tapdaq.com)

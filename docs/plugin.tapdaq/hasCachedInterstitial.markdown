# plugin.tapdaq.hasCachedInterstitial()

> --------------------- ------------------------------------------------------------------------------------------
> __Type__              [function][api.type.Function]
> __Library__           [tapdaq.*][plugin.tapdaq]
> __Return value__      [TYPE][api.type.Boolean]
> __Revision__          [REVISION_LABEL](REVISION_URL)
> __Keywords__          tapdaq, ads
> __Sample code__       
> __See also__          
> --------------------- ------------------------------------------------------------------------------------------


## Overview

Returns whether there are interstitials cached for the current orientation.

## Syntax

	tapdaq.hasCachedInterstitial()

## Examples

``````lua

print(tapdaq.hasCachedInterstitial()) --will print true/false


``````

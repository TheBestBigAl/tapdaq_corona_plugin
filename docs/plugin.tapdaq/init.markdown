# plugin.tapdaq.init()

> --------------------- ------------------------------------------------------------------------------------------
> __Type__              [function][api.type.Function]
> __Library__           [tapdaq.*][plugin.tapdaq]
> __Return value__      [TYPE][api.type.Table]
> __Revision__          [REVISION_LABEL](REVISION_URL)
> __Keywords__          tapdaq, ads
> __Sample code__       
> __See also__          
> --------------------- ------------------------------------------------------------------------------------------


## Overview

tapdaq.init() must be called before all other functions, in order to initialise the SDK.

## Syntax

	tapdaq.init{
		appID = "ENTER_YOUR_APP_ID",
		clientKey = "ENTER_YOUR_CLIENT_KEY", 
		listener = tapdaqListener,
	}

##### appID ~^(required)^~
_[Table][api.type.String]._ The application ID for your application.

##### clientKey ~^(required)^~
_[Table][api.type.String]._ The client key associated with your developer account.

##### listener ~^(required)^~
_[TYPE][api.type.Function]._ The listener function which will handle the API response.



## Examples

``````lua
local tapdaq = require "plugin.tapdaq"

local function tapdaqListener( event )
	for k, v in pairs( event ) do
		print( k, ":", v )
	end
end


-- Initialise tapdaq
tapdaq.init(
	{
		appID = "a123b456c789",
		clientKey = "abc-123-def-456-789", 
		listener = tapdaqListener,
	}
)

``````

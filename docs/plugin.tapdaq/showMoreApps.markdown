# plugin.tapdaq.showMoreApps()

> --------------------- ------------------------------------------------------------------------------------------
> __Type__              [function][api.type.Function]
> __Library__           [tapdaq.*][plugin.tapdaq]
> __Return value__      [TYPE][api.type.Table]
> __Revision__          [REVISION_LABEL](REVISION_URL)
> __Keywords__          tapdaq, ads
> __Sample code__       
> __See also__          
> --------------------- ------------------------------------------------------------------------------------------


## Overview

Request an interstitial advert - these are precached after calling init.

## Syntax

	tapdaq.showMoreApps()

## Examples

``````lua
local function tapdaqListener( event )
	if event.type == "moreApps" then
		if event.phase == "willDisplay" then
			--do something if needed e.g. pause game
		elseif event.phase == "didDisplay" then
			--ad is being shown
		elseif event.phase == "didClose" then
			--ad is closed
		elseif event.phase == "failed" then
			print(event.message)
		end
	end 
end

tapdaq.showMoreApps()

``````

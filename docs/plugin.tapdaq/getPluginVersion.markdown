# plugin.tapdaq.getPluginVersion()

> --------------------- ------------------------------------------------------------------------------------------
> __Type__              [function][api.type.Function]
> __Library__           [tapdaq.*][plugin.tapdaq]
> __Return value__      [TYPE][api.type.Table]
> __Revision__          [REVISION_LABEL](REVISION_URL)
> __Keywords__          tapdaq, ads
> __Sample code__       
> __See also__          
> --------------------- ------------------------------------------------------------------------------------------


## Overview

Fetches the current plugin version number (note this may not be the same as the SDK version number).

## Syntax

	tapdaq.getPluginVersion()


## Examples

``````lua
local function tapdaqListener( event )
	if event.type == "version" then
		print(event.version) --> "0.0.1"
	end 
end

tapdaq.getPluginVersion()

``````

local tapdaq = require( "plugin.tapdaq" );
local widget = require( "widget" );
widget.setTheme( "widget_theme_ios" );

-- The tapdaq listener function
local function tapdaqListener( event )
	for k, v in pairs( event ) do
		print( k, ":", v )
	end

	if event.type == "init" then
		print(event.success)
		print(event.isDebug)
	elseif event.type == "version" then
		print(event.version)
	elseif event.type == "interstitial" then
		if event.phase then
			if event.phase == "willDisplay" then
				--do something if needed e.g. pause game
			elseif event.phase == "didDisplay" then
				--ad is being shown
			elseif event.phase == "didClose" then
				--ad is closed
			elseif event.phase == "failed" then
				print(event.message)
			end
		end
	elseif event.type == "moreApps" then
		if event.phase then
			if event.phase == "willDisplay" then
				--do something if needed e.g. pause game
			elseif event.phase == "didDisplay" then
				--ad is being shown
			elseif event.phase == "didClose" then
				--ad is closed
			elseif event.phase == "failed" then
				print(event.message)
			end
		end
	end
end

-- Initialise tapdaq
tapdaq.init(
	{
		appID = "ENTER_YOUR_APP_ID",
		clientKey = "ENTER_YOUR_CLIENT_KEY", 
		listener = tapdaqListener,
	}
)

tapdaq.getPluginVersion()

-- Show Ad button
local showIntButton = widget.newButton
{
	label = "Show Interstitial";
	onRelease = function( event )
        print("releasing Show Interstitial button")
        if tapdaq.hasCachedInterstitial() then
        	tapdaq.showInterstitial()
        else
        	print("No interstitials currently cached.")
        end
	end,
}
showIntButton.x = display.contentCenterX;
showIntButton.y = 150;

-- Cache More Apps
local showMoreAppsButton = widget.newButton
{
	label = "Show More Apps",
	onRelease = function( event )
		print("releasing Show More Apps button")
		tapdaq.showMoreApps();
	end,
}
showMoreAppsButton.x = display.contentCenterX;
showMoreAppsButton.y = showIntButton.y + showIntButton.contentHeight + showMoreAppsButton.contentHeight * 0.5;